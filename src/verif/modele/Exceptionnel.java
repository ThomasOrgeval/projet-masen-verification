package verif.modele;

public class Exceptionnel {

    protected String id, commentaire, date;
    protected Utilisateur unUtilisateur;

    public Exceptionnel(String id, String commentaire, Utilisateur unUtilisateur, String date) {
        this.id = id;
        this.commentaire = commentaire;
        this.unUtilisateur = unUtilisateur;
        this.date = date;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public Utilisateur getUnUtilisateur() {
        return unUtilisateur;
    }

    public void setUnUtilisateur(Utilisateur unUtilisateur) {
        this.unUtilisateur = unUtilisateur;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
