package verif.modele;

public class Mensuel {

    protected String id, equipIndvdFlotta, radeau, assistance, date;
    protected Utilisateur unUtilisateur;

    public Mensuel(String id, String equipIndvdFlotta, String radeau, String assistance, Utilisateur unUtilisateur, String date) {
        this.id = id;
        this.equipIndvdFlotta = equipIndvdFlotta;
        this.radeau = radeau;
        this.assistance = assistance;
        this.unUtilisateur = unUtilisateur;
        this.date = date;
    }

    public String getEquipIndvdFlotta() {
        return equipIndvdFlotta;
    }

    public void setEquipIndvdFlotta(String equipIndvdFlotta) {
        this.equipIndvdFlotta = equipIndvdFlotta;
    }

    public String getRadeau() {
        return radeau;
    }

    public void setRadeau(String radeau) {
        this.radeau = radeau;
    }

    public String getAssistance() {
        return assistance;
    }

    public void setAssistance(String assistance) {
        this.assistance = assistance;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Utilisateur getUnUtilisateur() {
        return unUtilisateur;
    }

    public void setUnUtilisateur(Utilisateur unUtilisateur) {
        this.unUtilisateur = unUtilisateur;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
