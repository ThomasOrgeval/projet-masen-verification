package verif.modele;

public class Annuel {

    protected String id, date;
    protected int equipIndvdFlotta, dispoLum, incendie, radeau, vhf, assistance, trousse;
    protected Utilisateur unUtilisateur;

    public Annuel(String id, int equipIndvdFlotta, int dispoLum, int incendie, int radeau, int vhf, int assistance, int trousse, Utilisateur unUtilisateur, String date) {
        this.id = id;
        this.equipIndvdFlotta = equipIndvdFlotta;
        this.dispoLum = dispoLum;
        this.incendie = incendie;
        this.radeau = radeau;
        this.vhf = vhf;
        this.assistance = assistance;
        this.trousse = trousse;
        this.unUtilisateur = unUtilisateur;
        this.date = date;
    }

    public int getEquipIndvdFlotta() {
        return equipIndvdFlotta;
    }

    public void setEquipIndvdFlotta(int equipIndvdFlotta) {
        this.equipIndvdFlotta = equipIndvdFlotta;
    }

    public int getDispoLum() {
        return dispoLum;
    }

    public void setDispoLum(int dispoLum) {
        this.dispoLum = dispoLum;
    }

    public int getIncendie() {
        return incendie;
    }

    public void setIncendie(int incendie) {
        this.incendie = incendie;
    }

    public int getRadeau() {
        return radeau;
    }

    public void setRadeau(int radeau) {
        this.radeau = radeau;
    }

    public int getVhf() {
        return vhf;
    }

    public void setVhf(int vhf) {
        this.vhf = vhf;
    }

    public int getAssistance() {
        return assistance;
    }

    public void setAssistance(int assistance) {
        this.assistance = assistance;
    }

    public int getTrousse() {
        return trousse;
    }

    public void setTrousse(int trousse) {
        this.trousse = trousse;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Utilisateur getUnUtilisateur() {
        return unUtilisateur;
    }

    public void setUnUtilisateur(Utilisateur unUtilisateur) {
        this.unUtilisateur = unUtilisateur;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
