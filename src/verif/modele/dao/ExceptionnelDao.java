package verif.modele.dao;

import verif.modele.Exceptionnel;
import verif.modele.Utilisateur;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ExceptionnelDao {

    public static Exceptionnel rechercher(String id) {
        Exceptionnel uneException = null;
        Utilisateur unutilisateur;
        ResultSet reqSelection = ConnexionMySql.execReqSelection("select * from EXCEPTIONNEL where `id`='" + id + "'");
        try {
            if (reqSelection.next()) {
                unutilisateur = UtilisateurDao.rechercher(reqSelection.getInt(3));
                uneException = new Exceptionnel(reqSelection.getString(1), reqSelection.getString(2), unutilisateur, reqSelection.getString(4));
            }
        } catch (Exception e) {
            System.out.println("erreur reqSelection.next() pour la requête - select * from EXCEPTIONNEL where `id`='" + id + "'");
            e.printStackTrace();
        }
        ConnexionMySql.fermerConnexionBd();
        return uneException;
    }

    public static ArrayList<Exceptionnel> retournerCollectionExceptionnel() {
        ArrayList<Exceptionnel> colExceptionnel = new ArrayList<>();
        ResultSet reqSelection = ConnexionMySql.execReqSelection("select `id` from EXCEPTIONNEL");
        try {
            while (reqSelection.next()) {
                String id = reqSelection.getString(1);
                colExceptionnel.add(ExceptionnelDao.rechercher(id));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("erreur retournerCollectionExceptionnel()");
        }
        return colExceptionnel;
    }

    public static void creerException(Exceptionnel uneException) {
        ConnexionMySql.execReqMaj("insert into EXCEPTIONNEL values('" + uneException.getId() + "','" + uneException.getCommentaire() + "','" + uneException.getUnUtilisateur().getId() + "','" + uneException.getDate() + "')");
    }

    public static void modificationException(Exceptionnel uneException) {
        ConnexionMySql.execReqMaj("delete from EXCEPTIONNEL where `id`='" + uneException.getId() + "'");
        creerException(uneException);
    }
}
