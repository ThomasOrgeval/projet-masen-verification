package verif.modele.dao;

import verif.modele.Mensuel;
import verif.modele.Utilisateur;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class MensuelDao {

    public static Mensuel rechercher(String id) {
        Mensuel unMois = null;
        Utilisateur unutilisateur;
        ResultSet reqSelection = ConnexionMySql.execReqSelection("select * from MENSUEL where `id`='" + id + "'");
        try {
            if (reqSelection.next()) {
                unutilisateur = UtilisateurDao.rechercher(reqSelection.getInt(5));
                unMois = new Mensuel(reqSelection.getString(1), reqSelection.getString(2), reqSelection.getString(3), reqSelection.getString(4), unutilisateur, reqSelection.getString(6));
            }
        } catch (Exception e) {
            System.out.println("erreur reqSelection.next() pour la requête - select * from MENSUEL where `id`='" + id + "'");
            e.printStackTrace();
        }
        ConnexionMySql.fermerConnexionBd();
        return unMois;
    }

    public static ArrayList<Mensuel> retournerCollectionMensuel() {
        ArrayList<Mensuel> colMensuel = new ArrayList<>();
        ResultSet reqSelection = ConnexionMySql.execReqSelection("select `id` from MENSUEL");
        try {
            while (reqSelection.next()) {
                String id = reqSelection.getString(1);
                colMensuel.add(MensuelDao.rechercher(id));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("erreur retournerCollectionMensuel()");
        }
        return colMensuel;
    }

    public static void creerMois(Mensuel unMois) {
        ConnexionMySql.execReqMaj("insert into MENSUEL values('" + unMois.getId() + "','" + unMois.getEquipIndvdFlotta() + "','" + unMois.getRadeau() + "','" + unMois.getAssistance() + "','" + unMois.getUnUtilisateur().getId() + "','" + unMois.getDate() + "')");
    }

    public static void modificationMensuel(Mensuel unMois) {
        ConnexionMySql.execReqMaj("delete from MENSUEL where `id`='" + unMois.getId() + "'");
        creerMois(unMois);
    }
}
