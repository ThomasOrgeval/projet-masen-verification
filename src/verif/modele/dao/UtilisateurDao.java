package verif.modele.dao;

import verif.modele.Utilisateur;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UtilisateurDao {

    public static Utilisateur rechercher (int id) {
        Utilisateur unUtilisateur = null;
        ResultSet reqSelection = ConnexionMySql.execReqSelection("select * from USER where `id`='" + id + "'");
        try {
            if (reqSelection.next()) {
                unUtilisateur = new Utilisateur(reqSelection.getInt(1), reqSelection.getString(2), reqSelection.getString(3));
            }
        } catch (SQLException e) {
            System.out.println("erreur reqSelection.next() pour la requête - select * from USER where `id`='" + id + "'");
            e.printStackTrace();
        }
        ConnexionMySql.fermerConnexionBd();
        return unUtilisateur;
    }

    public static ArrayList<Utilisateur> retournerCollectionDesUtilisateurs() {
        ArrayList<Utilisateur> colUsers = new ArrayList<>();
        ResultSet reqSelection = ConnexionMySql.execReqSelection("select `id` from USER");
        try {
            while (reqSelection.next()) {
                int id = reqSelection.getInt(1);
                colUsers.add(UtilisateurDao.rechercher(id));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("erreur retournerCollectionDesUtilisateurs()");
        }
        return colUsers;
    }
}
