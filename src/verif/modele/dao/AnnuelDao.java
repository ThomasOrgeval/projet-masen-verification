package verif.modele.dao;

import verif.modele.Annuel;
import verif.modele.Utilisateur;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AnnuelDao {

    public static Annuel rechercher(String id) {
        Annuel uneAnnee = null;
        Utilisateur unutilisateur;
        ResultSet reqSelection = ConnexionMySql.execReqSelection("select * from ANNUEL where `id`='" + id + "'");
        try {
            if (reqSelection.next()) {
                unutilisateur = UtilisateurDao.rechercher(reqSelection.getInt(9));
                uneAnnee = new Annuel(reqSelection.getString(1), reqSelection.getInt(2), reqSelection.getInt(3), reqSelection.getInt(4), reqSelection.getInt(5), reqSelection.getInt(6), reqSelection.getInt(7), reqSelection.getInt(8), unutilisateur, reqSelection.getString(10));
            }
        } catch (Exception e) {
            System.out.println("erreur reqSelection.next() pour la requête - select * from ANNUEL where `id`='" + id + "'");
            e.printStackTrace();
        }
        ConnexionMySql.fermerConnexionBd();
        return uneAnnee;
    }

    public static ArrayList<Annuel> retournerCollectionAnnuel() {
        ArrayList<Annuel> colAnnuel = new ArrayList<>();
        ResultSet reqSelection = ConnexionMySql.execReqSelection("select `id` from ANNUEL");
        try {
            while (reqSelection.next()) {
                String id = reqSelection.getString(1);
                colAnnuel.add(AnnuelDao.rechercher(id));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("erreur retournerCollectionAnnuel()");
        }
        return colAnnuel;
    }

    public static void creerAnnuel(Annuel uneAnnee) {
        ConnexionMySql.execReqMaj("insert into ANNUEL values('" + uneAnnee.getId() + "','" + uneAnnee.getEquipIndvdFlotta() + "','" + uneAnnee.getDispoLum() + "','" + uneAnnee.getIncendie() + "','" + uneAnnee.getRadeau() + "','" + uneAnnee.getVhf() + "','" + uneAnnee.getAssistance() + "','" + uneAnnee.getTrousse() + "','" + uneAnnee.getUnUtilisateur().getId() + "','" + uneAnnee.getDate() + "')");
    }

    public static void modificationAnnuel(Annuel uneAnnee) {
        ConnexionMySql.execReqMaj("delete from ANNUEL where `id`='" + uneAnnee.getId() + "'");
        creerAnnuel(uneAnnee);
    }
}
