package verif.modele;

public class Utilisateur {

    protected int id;
    protected String nom, commentaire;

    public Utilisateur(int id, String nom, String commentaire) {
        this.id = id;
        this.nom = nom;
        this.commentaire = commentaire;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCommentaire() {
        return commentaire;
    }

    public void setCommentaire(String commentaire) {
        this.commentaire = commentaire;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
