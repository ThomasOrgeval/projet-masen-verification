package verif.tests;

import verif.vue.JIFUtilisateur;
import verif.vue.MenuPrincipal;

public class Application {

    public static void main(String[] args) {
        MenuPrincipal application;
        application = new MenuPrincipal();
        MenuPrincipal.ouvrirFenetre(new JIFUtilisateur());
    }

}