package verif.vue;

import verif.modele.Exceptionnel;
import verif.modele.Utilisateur;
import verif.modele.dao.ExceptionnelDao;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class JIFSaisieExceptionnelle extends JInternalFrame implements ActionListener {

    protected boolean modif = false;
    protected String id;
    protected JPanel pSpring, pLabel, pTextField, pBtn;
    protected SpringLayout spring = new SpringLayout();

    protected JButton JBvalider;
    protected JLabel JLsituation;
    protected JTextField JTsituation;

    public JIFSaisieExceptionnelle(String code) {
        // Création des panneaux
        Container contentPane = getContentPane();
        pSpring = new JPanel(spring); // panneau supportant les labels et les textes de 7 lignes et 2 colonnes
        pLabel = new JPanel();
        pTextField = new JPanel();
        pBtn = new JPanel();

        // Création du label
        JLsituation = new JLabel("Situation :");
        pLabel.add(JLsituation);

        // Création de la zone de texte
        JTsituation = new JTextField(35);
        JTsituation.addActionListener(this);
        pTextField.add(JTsituation);

        if (code != null) {
            remplitText(code);
            id = code;
            modif = true;
        }

        // Création du bouton valider
        JBvalider = new JButton("Valider");
        JBvalider.addActionListener(this);
        pBtn.add(JBvalider);

        // Contrainte du layout principal
        spring.putConstraint(SpringLayout.WEST, pLabel, 10, SpringLayout.WEST, contentPane);
        spring.putConstraint(SpringLayout.NORTH, pLabel, 5, SpringLayout.NORTH, contentPane);
        spring.putConstraint(SpringLayout.WEST, pTextField, 70, SpringLayout.WEST, pLabel);
        spring.putConstraint(SpringLayout.NORTH, pTextField, 5, SpringLayout.NORTH, contentPane);
        spring.putConstraint(SpringLayout.WEST, pBtn, 220, SpringLayout.WEST, contentPane);
        spring.putConstraint(SpringLayout.NORTH, pBtn, 100, SpringLayout.NORTH, pLabel);

        // Mise en forme de la fenêtre
        pSpring.add(pLabel);
        pSpring.add(pTextField);
        pSpring.add(pBtn);
        contentPane.add(pSpring);
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        Object source = evt.getSource();
        if (source == JBvalider || source == JTsituation) {
            if (modif) {
                Utilisateur unUser = new Utilisateur(1, "OUI", "TEST");
                Exceptionnel uneException = new Exceptionnel(id, JTsituation.getText(), unUser, "22/04");
                ExceptionnelDao.modificationException(uneException);
                JOptionPane.showMessageDialog(this, "L'exception a été modifié");
                MenuPrincipal.ouvrirFenetre(new JIFConsultation());
            } else {
                int nbr = ExceptionnelDao.retournerCollectionExceptionnel().size()+1;
                Utilisateur unUser = new Utilisateur(1, "OUI", "TEST");
                Exceptionnel uneException = new Exceptionnel(nbr+"E", JTsituation.getText(), unUser, "22/04");
                ExceptionnelDao.creerException(uneException);
                JOptionPane.showMessageDialog(this, "L'exception a été ajouté");
            }
        }
    }

    public void remplitText(String code) {
        Exceptionnel uneException = ExceptionnelDao.rechercher(code);
        JTsituation.setText(uneException.getCommentaire());
    }
}
