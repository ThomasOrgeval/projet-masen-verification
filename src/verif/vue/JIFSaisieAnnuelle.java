package verif.vue;

import verif.modele.Annuel;
import verif.modele.Utilisateur;
import verif.modele.dao.AnnuelDao;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class JIFSaisieAnnuelle extends JInternalFrame implements ActionListener {

    protected boolean modif = false;
    protected String id;
    protected JPanel p, pLabel, pTextField, pBtn;
    protected SpringLayout spring = new SpringLayout();

    protected JButton JBvalider;
    protected JLabel JLequip, JLlum, JLincendie, JLradeau, JLvhf, JLassist, JLtrousse;
    protected JTextField JTequip, JTlum, JTincendie, JTradeau, JTvhf, JTassist, JTtrousse;

    public JIFSaisieAnnuelle(String code) {
        // Création des panneaux
        Container contentPane = getContentPane();
        p = new JPanel(spring); // panneau supportant les labels et les textes de 7 lignes et 2 colonnes
        pLabel = new JPanel(new GridLayout(7, 1, 0, 4));
        pTextField = new JPanel(new GridLayout(7, 1));
        pBtn = new JPanel();

        // Création des labels
        JLequip = new JLabel("Equipement individuel de flottabilité");
        JLlum = new JLabel("Dispositif lumineux");
        JLincendie = new JLabel("Moyen de lutte contre incendie");
        JLradeau = new JLabel("Radeau de survie");
        JLvhf = new JLabel("VHF portative");
        JLassist = new JLabel("Dispositif de repérage et d’assistance pour personne à la mer");
        JLtrousse = new JLabel("Trousse de secours");

        // Création des zones de texte
        JTequip = new JTextField(3);
        JTlum = new JTextField();
        JTincendie = new JTextField();
        JTradeau = new JTextField();
        JTvhf = new JTextField();
        JTassist = new JTextField();
        JTtrousse = new JTextField();

        if (code != null) {
            remplitText(code);
            id = code;
            modif = true;
        }

        // Ajout de la touche entrée pour effectuer une action plus rapidement
        JTequip.addActionListener(this);
        JTlum.addActionListener(this);
        JTincendie.addActionListener(this);
        JTradeau.addActionListener(this);
        JTvhf.addActionListener(this);
        JTassist.addActionListener(this);
        JTtrousse.addActionListener(this);

        // Ajout des labels et des zones de texte sur un panneau
        pLabel.add(JLequip);
        pLabel.add(JLlum);
        pLabel.add(JLincendie);
        pLabel.add(JLradeau);
        pLabel.add(JLvhf);
        pLabel.add(JLassist);
        pLabel.add(JLtrousse);

        pTextField.add(JTequip);
        pTextField.add(JTlum);
        pTextField.add(JTincendie);
        pTextField.add(JTradeau);
        pTextField.add(JTvhf);
        pTextField.add(JTassist);
        pTextField.add(JTtrousse);

        // Ajout du bouton valider
        JBvalider = new JButton("Valider");
        JBvalider.addActionListener(this);
        pBtn.add(JBvalider);

        // Contrainte du layout principal
        spring.putConstraint(SpringLayout.WEST, pLabel, 10, SpringLayout.WEST, contentPane);
        spring.putConstraint(SpringLayout.NORTH, pLabel, 5, SpringLayout.NORTH, contentPane);
        spring.putConstraint(SpringLayout.WEST, pTextField, 450, SpringLayout.WEST, pLabel);
        spring.putConstraint(SpringLayout.NORTH, pTextField, 5, SpringLayout.NORTH, contentPane);
        spring.putConstraint(SpringLayout.WEST, pBtn, 220, SpringLayout.WEST, contentPane);
        spring.putConstraint(SpringLayout.NORTH, pBtn, 200, SpringLayout.NORTH, pLabel);

        // Mise en forme de la fenêtre
        p.add(pLabel);
        p.add(pTextField);
        p.add(pBtn);
        contentPane.add(p);
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        Object source = evt.getSource();
        if (source == JBvalider || source == JTequip || source == JTlum || source == JTincendie || source == JTradeau || source == JTvhf || source == JTassist || source == JTtrousse) {

            try {
                if (JTequip.getText().equals("")) {
                    throw new Exception("Données obligatoires manquantes");
                } else {
                    if (modif) {
                        Utilisateur unUser = new Utilisateur(1, "OUI", "TEST");
                        Annuel uneAnnee = new Annuel(id, Integer.parseInt(JTequip.getText()), Integer.parseInt(JTlum.getText()), Integer.parseInt(JTincendie.getText()), Integer.parseInt(JTradeau.getText()), Integer.parseInt(JTvhf.getText()), Integer.parseInt(JTassist.getText()), Integer.parseInt(JTtrousse.getText()), unUser, "22/04");
                        AnnuelDao.modificationAnnuel(uneAnnee);
                        JOptionPane.showMessageDialog(this, "L'année a été modifiée");
                        MenuPrincipal.ouvrirFenetre(new JIFConsultation());
                    } else {
                        int nbr = AnnuelDao.retournerCollectionAnnuel().size()+1;
                        Utilisateur unUser = new Utilisateur(1, "OUI", "TEST");
                        Annuel uneAnnee = new Annuel(nbr+"A", Integer.parseInt(JTequip.getText()), Integer.parseInt(JTlum.getText()), Integer.parseInt(JTincendie.getText()), Integer.parseInt(JTradeau.getText()), Integer.parseInt(JTvhf.getText()), Integer.parseInt(JTassist.getText()), Integer.parseInt(JTtrousse.getText()), unUser, "22/04");
                        AnnuelDao.creerAnnuel(uneAnnee);
                        JOptionPane.showMessageDialog(this, "L'année a été ajoutée");
                    }
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this, e);
            }
        }
    }

    public void remplitText(String code) {
        Annuel uneAnnee = AnnuelDao.rechercher(code);
        JTequip.setText(String.valueOf(uneAnnee.getEquipIndvdFlotta()));
        JTlum.setText(String.valueOf(uneAnnee.getDispoLum()));
        JTincendie.setText(String.valueOf(uneAnnee.getIncendie()));
        JTradeau.setText(String.valueOf(uneAnnee.getRadeau()));
        JTvhf.setText(String.valueOf(uneAnnee.getVhf()));
        JTassist.setText(String.valueOf(uneAnnee.getAssistance()));
        JTtrousse.setText(String.valueOf(uneAnnee.getTrousse()));
    }
}
