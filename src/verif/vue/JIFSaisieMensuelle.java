package verif.vue;

import verif.modele.Mensuel;
import verif.modele.Utilisateur;
import verif.modele.dao.MensuelDao;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Objects;

public class JIFSaisieMensuelle extends JInternalFrame implements ActionListener {

    protected boolean modif = false;
    protected String id;
    protected JPanel pSpring, pLabel, pComboBox, pBtn;
    protected SpringLayout spring = new SpringLayout();

    protected JButton JBvalider;
    protected JLabel JLequip, JLradeau, JLassist;
    protected JComboBox<String> JCequip, JCradeau, JCassist;

    public JIFSaisieMensuelle(String code) {
        // Création des panneaux
        Container contentPane = getContentPane();
        pSpring = new JPanel(spring); // panneau supportant les labels et les textes de 7 lignes et 2 colonnes
        pLabel = new JPanel(new GridLayout(4, 1, 0, 4));
        pComboBox = new JPanel(new GridLayout(4, 1));
        pBtn = new JPanel();

        // Création des labels
        JLequip = new JLabel("Equipement individuel de flottabilité");
        JLradeau = new JLabel("Radeau de survie");
        JLassist = new JLabel("Dispositif de repérage et d’assistance pour personne à la mer");

        // Création des zones de texte
        JCequip = new JComboBox<>(new String[]{"Neuf", "Très bon", "Bon", "Endommagé", "Hors service"});
        JCradeau = new JComboBox<>(new String[]{"Neuf", "Très bon", "Bon", "Endommagé", "Hors service"});
        JCassist = new JComboBox<>(new String[]{"Neuf", "Très bon", "Bon", "Endommagé", "Hors service"});

        // Ajout des labels et des zones de texte sur un panneau
        pLabel.add(JLequip);
        pLabel.add(JLradeau);
        pLabel.add(JLassist);

        pComboBox.add(JCequip);
        pComboBox.add(JCradeau);
        pComboBox.add(JCassist);

        if (code != null) {
            remplitText(code);
            id = code;
            modif = true;
        }

        // Ajout du bouton valider
        JBvalider = new JButton("Valider");
        JBvalider.addActionListener(this);
        pBtn.add(JBvalider);

        // Contrainte du layout principal
        spring.putConstraint(SpringLayout.WEST, pLabel, 10, SpringLayout.WEST, contentPane);
        spring.putConstraint(SpringLayout.NORTH, pLabel, 5, SpringLayout.NORTH, contentPane);
        spring.putConstraint(SpringLayout.WEST, pComboBox, 380, SpringLayout.WEST, pLabel);
        spring.putConstraint(SpringLayout.NORTH, pComboBox, 5, SpringLayout.NORTH, contentPane);
        spring.putConstraint(SpringLayout.WEST, pBtn, 220, SpringLayout.WEST, contentPane);
        spring.putConstraint(SpringLayout.NORTH, pBtn, 100, SpringLayout.NORTH, pLabel);

        // Mise en forme de la fenêtre
        pSpring.add(pLabel);
        pSpring.add(pComboBox);
        pSpring.add(pBtn);
        contentPane.add(pSpring);
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        Object source = evt.getSource();
        if (source == JBvalider) {
            if (modif) {
                Utilisateur unUser = new Utilisateur(1, "OUI", "TEST");
                Mensuel unMois = new Mensuel(id, Objects.requireNonNull(JCequip.getSelectedItem()).toString(), Objects.requireNonNull(JCradeau.getSelectedItem()).toString(), Objects.requireNonNull(JCassist.getSelectedItem()).toString(), unUser, "22/04");
                MensuelDao.modificationMensuel(unMois);
                JOptionPane.showMessageDialog(this, "Le mois a été modifié");
                MenuPrincipal.ouvrirFenetre(new JIFConsultation());
            } else {
                int nbr = MensuelDao.retournerCollectionMensuel().size()+1;
                Utilisateur unUser = new Utilisateur(1, "OUI",  "TEST");
                Mensuel unMois = new Mensuel(nbr+"M", Objects.requireNonNull(JCequip.getSelectedItem()).toString(), Objects.requireNonNull(JCradeau.getSelectedItem()).toString(), Objects.requireNonNull(JCassist.getSelectedItem()).toString(), unUser,"22/04");
                MensuelDao.creerMois(unMois);
                JOptionPane.showMessageDialog(this, "Le mois a été ajouté");
            }
        }
    }

    public void remplitText(String code) {
        Mensuel unMois = MensuelDao.rechercher(code);
        JCequip.setSelectedItem(unMois.getEquipIndvdFlotta());
        JCradeau.setSelectedItem(unMois.getRadeau());
        JCassist.setSelectedItem(unMois.getAssistance());
    }
}
