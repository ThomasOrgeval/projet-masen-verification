package verif.vue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.*;

public class MenuPrincipal extends JFrame implements ActionListener {

    private static JInternalFrame myJInternalFrame;
    private static JDesktopPane desktopPane;

    protected JMenuBar mbar;
    protected JMenu mVerif;

    public MenuPrincipal() {

        myJInternalFrame = new JInternalFrame(); // pour affichage d'une seule
        // JInternalFrame à la fois
        desktopPane = new JDesktopPane();
        desktopPane.setBackground(Color.gray);
        JPanel contentPane = (JPanel) this.getContentPane();
        contentPane.add(desktopPane, BorderLayout.CENTER);

        setTitle("Vérification");
        setSize(550, 400);

        // Ajout d'une barre de menus à la fenêtre
        mbar = new JMenuBar();
        mVerif = new JMenu("Vérifications");
        JMenuItem mV1 = new JMenuItem("Saisie d'une vérification annuelle");
        mV1.addActionListener(this); // installation d'un écouteur d'action
        mVerif.add(mV1);
        JMenuItem mV2 = new JMenuItem("Saisie d'une vérification mensuelle");
        mV2.addActionListener(this);
        mVerif.add(mV2);
        JMenuItem mV3 = new JMenuItem("Saisie d'une vérification exceptionnelle");
        mV3.addActionListener(this);
        mVerif.add(mV3);
        JMenuItem mV4 = new JMenuItem("Consultation d'une vérification");
        mV4.addActionListener(this);
        mVerif.add(mV4);

        mbar.add(mVerif);
        setJMenuBar(mbar);
        this.setVisible(true);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        if (evt.getSource() instanceof JMenuItem) {
            String ChoixOption = evt.getActionCommand();

            switch (ChoixOption) {
                case "Saisie d'une vérification annuelle":
                    ouvrirFenetre(new JIFSaisieAnnuelle(null));
                    break;
                case "Saisie d'une vérification mensuelle":
                    ouvrirFenetre(new JIFSaisieMensuelle(null));
                    break;
                case "Saisie d'une vérification exceptionnelle":
                    ouvrirFenetre(new JIFSaisieExceptionnelle(null));
                    break;
                case "Consultation d'une vérification":
                    ouvrirFenetre(new JIFConsultation());
                    break;
            }

        }

    }

    public static void ouvrirFenetre(JInternalFrame uneFenetre) {
        myJInternalFrame.dispose();

        myJInternalFrame = uneFenetre;
        myJInternalFrame.setVisible(true);
        myJInternalFrame.setResizable(true);
        myJInternalFrame.setMaximizable(true);
        myJInternalFrame.setClosable(true);
        myJInternalFrame.setSize(530, 337);
        desktopPane.add(myJInternalFrame);
    }

}
