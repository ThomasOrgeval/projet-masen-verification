package verif.vue;

import verif.modele.Annuel;
import verif.modele.Exceptionnel;
import verif.modele.Mensuel;
import verif.modele.dao.AnnuelDao;
import verif.modele.dao.ExceptionnelDao;
import verif.modele.dao.MensuelDao;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class JIFConsultation extends JInternalFrame implements ActionListener {

    protected ArrayList<Annuel> colAnnuel = AnnuelDao.retournerCollectionAnnuel();
    protected ArrayList<Mensuel> colMensuel = MensuelDao.retournerCollectionMensuel();
    protected ArrayList<Exceptionnel> colExcep = ExceptionnelDao.retournerCollectionExceptionnel();

    protected JLabel JLcode;
    protected JTextField JTcode;
    protected JButton JBmodif;
    protected DefaultTableModel data;

    public JIFConsultation() {

        Container contentPane = getContentPane();
        JPanel p = new JPanel();
        JPanel pText = new JPanel(new GridLayout(1, 2));
        JPanel pBtn = new JPanel();

        JLcode = new JLabel("Code :");
        JTcode = new JTextField(20);
        JTcode.addActionListener(this);
        pText.add(JLcode);
        pText.add(JTcode);

        JBmodif = new JButton("Modifier");
        JBmodif.addActionListener(this);
        pBtn.add(JBmodif);

        String[] columnNames = {"Code", "Type", "Date", "Utilisateur"};
        data = new DefaultTableModel(columnNames, 0);
        JTable pTableau = new JTable(data);
        pTableau.getSelectionModel().addListSelectionListener(pTableau);
        remplirTableau();

        JScrollPane scrollPane = new JScrollPane(pTableau);
        scrollPane.setPreferredSize(new Dimension(500, 100));

        p.add(scrollPane);
        p.add(pText);
        p.add(pBtn);
        contentPane.add(p);
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        Object source = evt.getSource();
        if (source == JBmodif || source == JTcode) {
            try {
                if (JTcode.getText().equals("")) {
                    throw new Exception("Champ du code vide");
                } else if (AnnuelDao.rechercher(JTcode.getText()) == null && MensuelDao.rechercher(JTcode.getText()) == null && ExceptionnelDao.rechercher(JTcode.getText()) == null) {
                    throw new Exception("Ce code n'existe pas");
                } else if (JTcode.getText().contains("A")) {
                    MenuPrincipal.ouvrirFenetre(new JIFSaisieAnnuelle(JTcode.getText()));
                } else if (JTcode.getText().contains("M")) {
                    MenuPrincipal.ouvrirFenetre(new JIFSaisieMensuelle(JTcode.getText()));
                } else if (JTcode.getText().contains("E")) {
                    MenuPrincipal.ouvrirFenetre(new JIFSaisieExceptionnelle(JTcode.getText()));
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this, e);
            }
        }
    }

    private void remplirTableau() {
        String[] donnee = new String[4];
        for (Annuel uneAnnee : colAnnuel) {
            donnee[0] = String.valueOf(uneAnnee.getId());
            donnee[1] = "Annuel";
            donnee[2] = uneAnnee.getDate();
            donnee[3] = uneAnnee.getUnUtilisateur().getNom();
            data.addRow(donnee);
        }
        for (Mensuel unMois : colMensuel) {
            donnee[0] = String.valueOf(unMois.getId());
            donnee[1] = "Mensuel";
            donnee[2] = unMois.getDate();
            donnee[3] = unMois.getUnUtilisateur().getNom();
            data.addRow(donnee);
        }
        for (Exceptionnel uneException : colExcep) {
            donnee[0] = String.valueOf(uneException.getId());
            donnee[1] = "Exceptionnel";
            donnee[2] = uneException.getDate();
            donnee[3] = uneException.getUnUtilisateur().getNom();
            data.addRow(donnee);
        }
    }
}
