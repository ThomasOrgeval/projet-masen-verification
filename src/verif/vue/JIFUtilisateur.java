package verif.vue;

import verif.modele.Utilisateur;
import verif.modele.dao.UtilisateurDao;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class JIFUtilisateur extends JInternalFrame implements ActionListener {

    public Utilisateur user;
    protected ArrayList<Utilisateur> users = UtilisateurDao.retournerCollectionDesUtilisateurs();

    protected JPanel p, pText, pComboBox, pBtn1, pBtn2;
    protected JComboBox<String> JCusers;
    protected JButton JBajouterUser, JBselectUser;
    protected JLabel JLnom, JLselect;
    protected JTextField JTnom;

    public JIFUtilisateur() {
        // Création des panneaux
        Container contentPane = getContentPane();
        p = new JPanel();
        pText = new JPanel(new GridLayout(1, 2));
        pBtn1 = new JPanel(new GridLayout(1, 1));
        pComboBox = new JPanel(new GridLayout(1,2));
        pBtn2 = new JPanel(new GridLayout(1,1));

        // Création de la première partie
        JLnom = new JLabel("Nom de l'utilisateur :");
        JTnom = new JTextField(20);
        JTnom.addActionListener(this);
        pText.add(JLnom);
        pText.add(JTnom);

        JBajouterUser = new JButton("Ajouter l'utilisateur");
        JBajouterUser.addActionListener(this);
        pBtn1.add(JBajouterUser);

        // Instanciation des combobox
        JLselect = new JLabel("Sélection : ");
        String[] arrayUsers = new String[users.size()];
        int i = 0;
        for (Utilisateur unUser : users) {
            arrayUsers[i] = unUser.getNom();
            i++;
        }
        JCusers = new JComboBox<>(arrayUsers);
        pComboBox.add(JLselect);
        pComboBox.add(JCusers);

        JBselectUser = new JButton("Sélectionner l'utilisateur");
        JBselectUser.addActionListener(this);
        pBtn2.add(JBselectUser);

        // Mise en forme de la fenêtre
        p.add(pText);
        p.add(pBtn1);
        p.add(pComboBox);
        p.add(pBtn2);
        contentPane.add(p);
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        Object source = evt.getSource();
        if (source == JBajouterUser || source == JTnom) {

        } else if (source == JBselectUser) {

        }
    }
}
