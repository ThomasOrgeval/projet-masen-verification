drop database if exists masen;
create database masen;
use masen;

create table `USER`
(
    `id`          int auto_increment not null,
    `name`        varchar(255)       not null,
    `commentaire` varchar(255),
    primary key (`id`)
) engine = InnoDB;

create table `MENSUEL`
(
    `id`     varchar(4)   not null,
    `equip`  varchar(255) not null,
    `radeau` varchar(255) not null,
    `assist` varchar(255) not null,
    `user`   int          not null,
    `date`        varchar(255)       not null,
    primary key (`id`)
) engine = InnoDB;

create table `ANNUEL`
(
    `id`       varchar(4) not null,
    `equip`    int        not null,
    `lum`      int        not null,
    `incendie` int        not null,
    `radeau`   int        not null,
    `vhf`      int        not null,
    `assist`   int        not null,
    `trousse`  int        not null,
    `user`     int        not null,
    `date`        varchar(255)       not null,
    primary key (`id`)
) engine = InnoDB;

create table `EXCEPTIONNEL`
(
    `id`          varchar(4) not null,
    `commentaire` longtext   not null,
    `user`        int        not null,
    `date`        varchar(255)       not null,
    primary key (`id`)
) engine = InnoDB;

alter table `MENSUEL`
    add constraint foreign key (`user`) references `USER` (`id`);
alter table `ANNUEL`
    add constraint foreign key (`user`) references `USER` (`id`);
alter table `EXCEPTIONNEL`
    add constraint foreign key (`user`) references `USER` (`id`);

insert into USER (name, commentaire)
values ('test', 'oui');

insert into MENSUEl (id, equip, radeau, assist, user, date)
VALUES ('1M', 'OUI', 'NON', 'OUI', 1, '22/04');